﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

namespace log4NetExample
{
    public partial class log4netPage : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(log4netPage).FullName);
        protected void Page_Load(object sender, EventArgs e)
        {            
            Session["Usuario"] = "log4Net";
            log.Debug("Debug Message");
            log.Info("Debug Message");
            log.Warn("Debug Message");
            log.Fatal("Fatal error", new Exception("Fatal error generated"));
            log.Error("Error message", new Exception("Error message generated"));
            Response.Write("<H1>Log Generated Successfully...</H1>");
        }
    }
}